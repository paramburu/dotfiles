# set formats
# %b - branchname
# %u - unstagedstr (see below)
# %c - stagedstr (see below)
# %a - action (e.g. rebase-i)
# %R - repository path
# %S - path in the repository

autoload -Uz vcs_info add-zsh-hook

PR_RST="%{${reset_color}%}"
FMT_BRANCH="%u%c%{$fg_bold[blue]%}(%b)${PR_RST}"
FMT_ACTION="(%{$fg[green]%}%a${PR_RST})"
FMT_UNSTAGED="%{$fg[red]%}●"
FMT_STAGED="%{$fg[green]%}●"

zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*:prompt:*' unstagedstr   "${FMT_UNSTAGED}"
zstyle ':vcs_info:*:prompt:*' stagedstr     "${FMT_STAGED}"
zstyle ':vcs_info:*:prompt:*' actionformats "${FMT_BRANCH}${FMT_ACTION}"
zstyle ':vcs_info:*:prompt:*' formats       "${FMT_BRANCH}"
zstyle ':vcs_info:*:prompt:*' nvcsformats   ""

function peter_precmd {
    print -rP "%{$fg[magenta]%}%n%{$reset_color%} at %{$fg[yellow]%}%M%{$reset_color%} in %{$fg_bold[green]%}%~%{$reset_color%}"
    vcs_info 'prompt'
}

add-zsh-hook precmd peter_precmd

PROMPT=$'%% '

RPROMPT=$'${vcs_info_msg_0_}'

# function zle-line-init zle-keymap-select {
#     RPS1="${${KEYMAP/vicmd/-- NORMAL --}/(main|viins)/-- INSERT --}"
#     RPS2=$RPS1
#     zle reset-prompt
# }
#
# zle -N zle-line-init
# zle -N zle-keymap-select
