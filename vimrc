 set nocompatible
 filetype off

" Vundle ------------------------------------------------------------------ {{{

    set rtp+=~/.vim/bundle/vundle/
    call vundle#rc()

    Bundle 'gmarik/vundle'

    " ColorSchemes
    Bundle 'tomasr/molokai'

    " Functionality
    Bundle 'editorconfig/editorconfig-vim'
    Bundle 'kien/ctrlp.vim'
    Bundle 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
    Bundle 'ervandew/supertab'
    Bundle 'tpope/vim-surround'

    " Language Specific
    Bundle 'klen/python-mode'
    Bundle 'davidhalter/jedi-vim'
    Bundle 'othree/html5.vim'
    Bundle 'miripiruni/CSScomb-for-Vim'

" }}}
" Basics ------------------------------------------------------------------ {{{

    filetype plugin indent on

    set mouse=a
    set autoindent
    set noshowmode
    set showcmd
    set visualbell
    set ttyfast
    set backspace=2
    set number
    set laststatus=2
    set history=1000
    set undofile
    set undoreload=1000
    set list
    set listchars=tab:▸\ ,eol:¬,extends:❯,precedes:❮
    set matchtime=3
    set showbreak=↪
    set splitbelow
    set splitright
    set autoread
    set shiftround
    set title
    set linebreak

    " Timeout key codes not mappings
    set notimeout
    set ttimeout
    set ttimeoutlen=10

    " Completion
    set complete=.,w,b,u,t
    set completeopt=longest,menuone,preview

    " Leader Key
    let mapleader=","
    let maplocalleader="\\"

    " Highligh trailing whitespace {{{

    autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
    au InsertLeave * match ExtraWhitespace /\s\+$/

    " }}}
    " Wildmenu completion {{{

    set wildmenu
    set wildmode=list:longest

    set wildignore+=.hg,.git,.svn                    " Version control
    set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg   " binary images
    set wildignore+=*.o,*.obj,*.exe,*.dll " compiled object files
    set wildignore+=*.spl                            " compiled spelling word lists
    set wildignore+=*.sw?                            " Vim swap files
    set wildignore+=*.DS_Store                       " OSX bullshit

    set wildignore+=migrations                       " Django migrations
    set wildignore+=*.pyc                            " Python byte code

    set wildignore+=*.orig                           " Merge resolution files

    " }}}
    " Line Return {{{
    " Make sure Vim returns to the same line when you reopen a file.

    augroup line_return
        au!
        au BufReadPost *
            \ if line("'\"") > 0 && line("'\"") <= line("$") |
            \     execute 'normal! g`"zvzz' |
            \ endif
    augroup end

    " }}}
    " Tabs, spaces & wrapping {{{

    set tabstop=4
    set softtabstop=4
    set shiftwidth=4
    set expandtab
    set nowrap
    set textwidth=79
    set colorcolumn=+1

    " }}}
    " Backup, swap & undo files {{{

    set nobackup
    set nowritebackup
    set noswapfile
    set undodir=~/.vim/tmp/undo//

    " }}}
    " Color Scheme {{{

    syntax on
    let g:molokai_original = 1
    let g:rehash256=1
    colorscheme molokai
    highlight ColorColumn ctermbg=233

    " }}}
    " Folding {{{

    set foldlevelstart=20
    highlight Folded ctermbg=235 ctermfg=brown
    highlight FoldColumn ctermbg=darkgrey ctermfg=white

    " }}}

" }}}
" Mappings ---------------------------------------------------------------- {{{

    imap jj <Esc>
    noremap <silent> <C-s> :update<CR>
    vnoremap <silent> <C-s> <C-c>:update<CR>
    inoremap <silent> <C-s> <C-o>:update<CR>

    " Navigation {{{ 

    noremap j gj
    noremap k gk
    noremap gj j
    noremap gk k

    noremap <C-h> <C-w>h
    noremap <C-j> <C-w>j
    noremap <C-k> <C-w>k
    noremap <C-l> <C-w>l

    " }}}
    " Text formatting {{{

    nnoremap Q gqip
    vnoremap Q gq

    " }}}
    " Indentation {{{

    nnoremap <Tab> >>_
    nnoremap <S-Tab> <<_
    "inoremap <S-Tab> <C-d>
    vnoremap <Tab> >gv
    vnoremap <S-Tab> <gv

    " }}}
    " Remove 'search highlighting' {{{

    noremap <silent> <leader>h :nohlsearch<CR>
    vnoremap <silent> <leader>h <C-c>:nohlsearch<CR>
    inoremap <silent> <leader>h <C-o>:nohlsearch<CR>

    "}}}

" }}}
" Search & Replace -------------------------------------------------------- {{{

    set ignorecase
    set smartcase
    set incsearch
    set showmatch
    set hlsearch
    "set gdefault

" }}}
" FileTypes --------------------------------------------------------------- {{{

    " Vim {{{

    augroup ft_vim
        au!

        au FileType vim setlocal foldmethod=marker
    augroup end

    " }}}
    " HTML {{{
    augroup ft_html
        au!

        au FileType *.html
                    \ if &omnifunc != '' |
                    \    call SuperTabChain(&omnifunc, "<c-x><c-o>") |
                    \ endif
    augroup end
    " }}}

" }}}
" Bundles settings -------------------------------------------------------- {{{

    " Supertab {{{

    let g:SuperTabDefaultCompletionType = "context"
    let g:SuperTabContextDefaultCompletionType = "<c-n>"
    let g:SuperTabLongestEnhanced = 1
    let g:SuperTabLongestHighlight = 1

    " }}}
    " Python-Mode {{{

    let g:pymode_doc = 0
    let g:pymode_rope = 0
    let g:pymode_folding = 1

    " }}}
    " Jedi {{{

    let g:jedi#popup_select_first = 1

    " }}}

" }}}
" Training ---------------------------------------------------------------- {{{
    map <Up> <nop>
    map <Right> <nop>
    map <Down> <nop>
    map <Left> <nop>
" }}}
